--Code produced by William Akins - 1959373
--Number of patent submissions
totalPatents :: Int
totalPatents = 10

--String array stores the different skills levels that referees have
expertiseLevels :: [String]
expertiseLevels = ["Exert", "Knowledgeable", "Familiar", "Inexpert"]

--list of the referees and their skill level - element0 = patentID, element1 = RefereeID, element2 = expertise level
referees :: [[[Int]]]
referees = [
  [[0, 0, 0], [0, 1, 3], [0, 2, 1], [0, 3, 2], [0, 4, 0]],
  [[1, 0, 1], [1, 1, 2], [1, 2, 2], [1, 3, 3], [1, 4, 1]],
  [[2, 0, 0], [2, 1, 2], [2, 2, 1], [2, 3, 0], [2, 4, 2]],
  [[3, 0, 2], [3, 1, 0], [3, 2, 3], [3, 3, 1], [3, 4, 3]],
  [[4, 0, 2], [4, 1, 3], [4, 2, 1], [4, 3, 1], [4, 4, 1]],
  [[5, 0, 0], [5, 1, 2], [5, 2, 3], [5, 3, 0], [5, 4, 2]],
  [[6, 0, 0], [6, 1, 2], [6, 2, 0], [6, 3, 1], [6, 4, 0]],
  [[7, 0, 1], [7, 1, 0], [7, 2, 2], [7, 3, 2], [7, 4, 2]],
  [[8, 0, 0], [8, 1, 2], [8, 2, 0], [8, 3, 3], [8, 4, 1]],
  [[9, 0, 1], [9, 1, 1], [9, 2, 3], [9, 3, 2], [9, 4, 0]]]

--find the skill level of a value passed in and return as text
getRefereeSkill :: Int -> String
getRefereeSkill x =
  expertiseLevels !! x

--Output the current patent ID to the console with some text
outputPatent :: Int -> IO()
outputPatent patentNum =
    putStrLn("Patent " ++ show patentNum)

--based on the current Referee and patent, check whether the current field in the referees list is "Familiar"
--This then returns a list with the value found appended to it, acting as a list that counts the number of "Familiar" instances found for all referees
checkIfFamiliar:: Int -> Int -> [Int] -> [Int]
checkIfFamiliar curPatent curReferee countList =
  if expertiseLevels !! (((referees !! curPatent) !! curReferee) !! 2) == "Familiar"
    then
      curReferee : countList
    else
      countList

--The data from the "Familiar" list is used to calculate the number of "familiar" instances occured for a single referee
--The filter function returns the instances of "Familiar" for a single referee and then using length on that returns the number of "Familiar" instances that occured
--This value is compared against k, returning true if it exceeds or equals the value and false if not
limitK:: Eq a => Int -> [a] -> a -> Bool
limitK k countList curReferee =
  if length (filter (==curReferee) countList) >= k
    then
      True
    else
      False

--Using the current referee and patent, evulate whether the current referee is an "Inexpert" or not
checkIfInexpert :: Int -> Int -> Bool
checkIfInexpert curPatent curReferee =
  if expertiseLevels !! (((referees !! curPatent) !! curReferee) !! 2) == "Inexpert"
    then
      True
    else
      False

--verifyReferee is used to determine whether to append the new referee to the list containing the different referees for a particular patent
--uses the results from the two checking functions
verifyReferee:: Int -> Int -> [Int] -> Bool -> Bool -> [Int]
verifyReferee curPatent curReferee refereeList isInexpert isFamiliar =
  if (isInexpert == False && isFamiliar == False)
    then 
      (((referees !! curPatent) !! curReferee) !! 1) : refereeList
    else
      refereeList

--use a recursive call to loop through each of the referees going from 0 - n
--for each referee perform the various function calls to verify whether this particular referee should be added to the main list that holds
--the referees associated with this particular patent
chooseReferees :: Int -> Int -> Int -> Int -> [Int] -> t -> [Int] -> [Int]
chooseReferees n k curPatent curReferee refereeList totalRefereeList countList = 
  if curReferee < n
    then do
      let isInexpert = checkIfInexpert curPatent curReferee

      let newCountList = checkIfFamiliar curPatent curReferee countList
      let isFamiliar = limitK k newCountList curReferee
      
      let x = verifyReferee curPatent curReferee refereeList isInexpert isFamiliar


      chooseReferees n k curPatent (curReferee + 1) x totalRefereeList newCountList
    else
      refereeList

--recursively loop through each referee in the main referee list for this patent
--output the correct referee data to the console, including their associated skill level for the patent
outputReferee :: Int -> [Int] -> Int -> IO()
outputReferee curReferee selectedReferees curPatent =
  if curReferee < length selectedReferees
    then do
      putStrLn("Referee " ++ show (selectedReferees !! curReferee) ++ " - " ++ getRefereeSkill (((referees !! curPatent) !! (selectedReferees !! curReferee)) !! 2))
      outputReferee (curReferee + 1) selectedReferees curPatent
    else
      return()

--the main output data function which recursively loops through each of the patents
--the main function calls are performed here that allow the processing of all other data 
outputData :: Int -> t -> Int -> Int -> [[Int]] -> IO()
outputData n m k curPatent totalRefereeList =
  if curPatent < totalPatents
    then do
      --referees associated with this patent get stored within the refereeList
      let refereeList = []

      let x = chooseReferees n k curPatent 0 refereeList totalRefereeList []

      let newTotalRefereeList = totalRefereeList ++ [x] :: [[Int]]

      outputPatent curPatent
      outputReferee 0 x curPatent
      putStr "\n\n"

      outputData n m k (curPatent + 1) newTotalRefereeList
    else
      return()

main :: IO ()
main = do
    putStrLn("Enter Value n (Number of technical members) from 1 to 5:")
    input <- getLine           
    let n = read input :: Int  -- convert the read line to an int

    putStrLn("Enter Value m (Workload variance) from 1 to 3:")
    input <- getLine           
    let m = read input :: Int  -- convert the read line to an int

    putStrLn("Enter Value k (Number of allowed Familiar submissions per referee) from 1 to 3:")
    input <- getLine           
    let k = read input :: Int  -- convert the read line to an int
  
    --Testing values
    --let n = 5
    --let m = 2
    --let k = 2


    outputData n m k 0 []
    putStr "\n"